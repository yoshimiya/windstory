package enemy;

import character.Syujinkou;

public class Army{
	int hp=50;
	int attack_s=40;

	public int getAttack_s() {
		return attack_s;
	}

	public void setAttack_s(int attack_s) {
		this.attack_s = attack_s;
	}

	public void attack_a(Syujinkou s){
		s.setHp(s.getHp() -40);
		System.out.println(this.attack_s+"のダメージを受ける\n残りHP"+s.getHp()+"\n");
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}
}
