package main;

import java.awt.BorderLayout;
import java.awt.Container;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Image_main extends JFrame{
	public static void main(String[] args) {
		Image_main frame = new Image_main(" 風の生れる場所 - Area Of First Wind -");
		   frame.setVisible(true);
		 }

	public Image_main(String title){
		    setTitle(title);
		    setBounds(100, 100, 800, 1000);
		    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		    JPanel p = new JPanel();

		    ImageIcon icon1 = new ImageIcon("./desert.jpg");
		    ImageIcon icon2 = new ImageIcon("./w_story.jpg");
		    ImageIcon icon3 = new ImageIcon("./wind_title.jpg");
		    System.out.println(icon1);

		    JLabel label1 = new JLabel(icon1);

		    JLabel label2 = new JLabel();
		    label2.setIcon(icon2);

		    JLabel label3 = new JLabel();
		    label3.setIcon(icon3);

		    p.add(label1);
		    p.add(label2);
		    p.add(label3);
		    Container contentPane = getContentPane();
		    contentPane.add(p, BorderLayout.CENTER);
		  }


}
